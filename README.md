------------------
EVAL ADM_CO
------------------

J'ai tout d'abord coder la classe Grid dans dans le package robot qui contient le module MAP.
Cette classe m'a pris du temps, trop de temps... 
Ce qui fait que le temps que je code un fichier My_test ensuite afin de tester ma classe il était déjà 11h15.

En lançant le pytest j'ai eu plusieurs erreurs que je n'ai pu modifier car sinon je n'aurais pas eu le temps d'expliquer un minimum le ce fichier README.

J'ai cependant crée un fichier .yml et un setup.py comme conseillé dans le tuto de Mr Saradaryan.

J'ai modifié ces deux fichiers par rapport au tuto en fonction bien évidemment des packages robot et Test.

Je n'ai donc pu effectuer l'intégration continue comme je le souhaitais comme durant le rendu projet final car faute de temps aujourd'hui.

Ceci s'explique par le fait que je ne suis pas très à l'aise en python donc je n'ai pu aller au bout de l'intégration continue.

La différence avec le rendu final c'est que l'on n'était pas pressé je pouvais donc passer le temps qu'il fallait sur le code.

Je n'ai pu tester les différentes étapes des pipelines car le pytest (première étape) n'était pas bon..

J'aurais bien aimé avoir le temps de lancer plusieurs test unitaire afin de corriger mes erreurs pour ensuite lancer la réalisation des différentes étapes de pipelines.

J'aurais voulu pouvoir, durant ce TP, plus m'attarder sur l'intégration continue...


