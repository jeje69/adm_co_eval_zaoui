# -*- coding: utf-8 -*-

""" 
Created on 07/05
by Jérémy Zaoui
"""

class Grid:
    def __init__(self, nb_lignes, nb_colonnes):
        self._grid = [[]]
        self._nb_lignes = nb_lignes
        self._nb_colonnes = nb_colonnes
        self._index[2] = [0,0]
        
    def down(self):
        new_grid = []
        new_grid.append(self._grid()[2])
        new_grid.append(self._grid()[1])
        new_grid.append(self._grid()[0])
        
        self._grid = new_grid.copy()
        
    def up(self):
        new_grid = []
        new_grid.append(self._grid()[0])
        new_grid.append(self._grid()[1])
        new_grid.append(self._grid()[2])
        
        self._grid = new_grid.copy()
        
    def right(self):
        new_grid = []
        new_grid.append([self._grid()[0][2], self._grid()[0][1], self._grid()[0][0]])
        new_grid.append([self._grid()[1][2], self._grid()[1][1], self._grid()[1][0]])
        new_grid.append([self._grid()[2][2], self._grid()[2][1], self._grid()[2][0]])
        
        self._grid = new_grid.copy()
        
    def left(self):
        new_grid = []
        new_grid.append([self._grid()[0][0], self._grid()[0][1], self._grid()[0][2]])
        new_grid.append([self._grid()[1][0], self._grid()[1][1], self._grid()[1][2]])
        new_grid.append([self._grid()[2][0], self._grid()[2][1], self._grid()[2][2]])
        
        self._grid = new_grid.copy()
        
    #def print(self):
    
    
    def main():
        G1 = Grid(3,3)
        G1.up()
        
    

