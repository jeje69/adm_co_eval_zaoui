import setuptools
from distutils.core import setup


setup(
    name='Grid',
    version='0.1dev',
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    author="jeje69",
    author_email="jeremy.zaoui@cpe.fr",
    description="robot",
    long_description=open('README.txt').read(),
    package_dir={"": "robot/MAP"},
    packages=setuptools.find_namespace_packages(where="robot/MAP"),
)
