# -*- coding: utf-8 -*-

""" 
Created on 07/05
by Jérémy Zaoui
"""

import unittest
import logging

# MAIN---------------------------------------------------------------
#Don't forget to setup the PYTHONPATH every time by using the command export PYTHONPATH=$PYTHONPATH:"/home/tp/adm_co_eval_zaoui/test/MAP"

from robot.MAP.Grid import Grid

class Test(unittest.TestCase):

    def test_down(self):
    
        test = Grid (3,3)
        result = test.down()
        print(result)
        self.assertEqual(result)
        
    def test_up(self):
    
        test = Grid(3,3)
        result = test.up()
        print (result)
        self.assertEqual(result)
    
    def test_right(self):
    
        test = Grid(3,3)
        result = test.right()
        print (result)
        self.assertEqual(result)
        
    def test_left(self):
    
        test = Grid(3,3)
        result = test.left()
        print (result)
        self.assertEqual(result)
    
    
    

if __name__ == '__main__': 
    unittest.main()
        
        

